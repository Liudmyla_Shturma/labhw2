const express = require('express');
const morgan = require('morgan');
const app = express();
const authRouter = require('./routers/authRouter');
const userRouter = require('./routers/userRouter');
const noteRouter = require('./routers/noteRouter');

app.use(express.json());
//app.use(express.static('build'));
//app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users', userRouter);
app.use('/api/notes', noteRouter);

module.exports = app;