const express = require('express');
const router = express.Router();

const { asyncWrapper } = require('./helpers');
const { getInfo, deleteUser, changePassword } = require('../controllers/userController');
const { authMiddleware } = require('./middlewares/authMiddleware');

router.use(asyncWrapper(authMiddleware));

router
   .route('/me')
   .get(asyncWrapper(getInfo))
   .patch(asyncWrapper(changePassword))
   .delete(asyncWrapper(deleteUser));

module.exports = router;