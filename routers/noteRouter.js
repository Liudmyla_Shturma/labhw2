const express = require('express');
const router = express.Router();

const { asyncWrapper } = require('./helpers');
const { getNotes, addNote, getNote, updateNote, changeNoteCheck, deleteNote } = require('../controllers/noteController');
const { authMiddleware } = require('./middlewares/authMiddleware');

router.use(asyncWrapper(authMiddleware));

router
   .route('/')
   .get(asyncWrapper(getNotes))
   .post(asyncWrapper(addNote));

router
   .route('/:id')
   .get(asyncWrapper(getNote))
   .put(asyncWrapper(updateNote))
   .patch(asyncWrapper(changeNoteCheck))
   .delete(asyncWrapper(deleteNote));

module.exports = router;