const jwt = require('jsonwebtoken');
const { JWT_SECRET } = require('../../config');

module.exports.authMiddleware = async (req, res, next) => {
    const header = req.headers['authorization'];

    if (!header) {
        res.status(401).json({message: `No Authorization http header found!`});
        return
    }

    const [tokenType, token] = header.split(' ');

    if (!token) {
        res.status(401).json({message: `No JWT token found!`});
        return
    }

    req.user = jwt.verify(token, JWT_SECRET);
    next();
}