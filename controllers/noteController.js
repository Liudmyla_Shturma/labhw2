const { Note } = require('../models/noteModel');
const APIFeatures = require('./../utils/apiFeatures');

exports.addNote = async (req, res) => {

  const { text } = req.body;

  const note = new Note({
        userId:req.user._id,
        completed:false,
        text:text
  });

  await note.save();

  res.json({message: 'Sucess'});

};

exports.getNote = async (req, res) => {
   const note = await Note.findById(req.params.id);
   if (note) {
    res.json({message: 'Success', note: note});
   }
   
   res.status(400).json({message: 'Failure'});
};

exports.updateNote = async (req, res) => {
  
  const note = await Note.findById(req.params.id).select('+text');

  note.text = req.body.text; 

  updatedNote = await note.save();

  res.status(200).json({
    status: 'success',
    data: {
      user: updatedNote
    }
  });

};


exports.getNotes = async (req, res) => {
  let filter = {};
  filter = { userId: req.user._id };

  const features = new APIFeatures(Note.find(filter), req.query)
    .filter()
    .sort()
    .limitFields()
    .paginate();
  
    const notes = await features.query;
  
  res.status(200).json({
    status: 'success',
    results: notes.length,
    notes: notes
  });
  
};

exports.deleteNote = async (req, res) => {
   const isdel = await Note.findByIdAndDelete(req.params.id);

   if (!isdel) {
      res.status(400).json({message: 'Failure'});
   } else {
    res.json({message: 'Success'});
   }

};


exports.changeNoteCheck = async (req, res) => {
  const note = await Note.findById(req.params.id).select('+completed');

  note.completed =  !note.completed;
  await note.save();

  res.json({message: 'Success'});
};