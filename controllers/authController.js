//const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const { JWT_SECRET } = require('../config');
const { User } = require('../models/userModel');

module.exports.registration = async (req, res) => {
    const { username,  password } = req.body;

    const user = new User({
        username,
        password
    });

    await user.save();

    res.json({message: 'User created successfully!'});
};

module.exports.login = async (req, res) => {

    const { username, password } = req.body;

    const user = await User.findOne({username});

    if (!user) {
        res.status(400).json({message: `No user with username '${username}' found!`});
        return
    }

    if ( password.localeCompare(user.password) != 0 ) {
        res.status(400).json({message: `Wrong password!`});
        return
    }

    const jwt_token = jwt.sign({ username: user.username, _id: user._id }, JWT_SECRET);
    res.json({message: 'Success', jwt_token});

};